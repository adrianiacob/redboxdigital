<?php
/** @var Mage_Customer_Model_Entity_Setup $installer */
$installer = $this;

$installer->startSetup();


$entityTypeId = $installer->getEntityTypeId('customer');
$attributeSetId = $installer->getDefaultAttributeSetId($entityTypeId);
$attributeGroupId = $installer->getDefaultAttributeGroupId(
    $entityTypeId, $attributeSetId
);

$attributeData = [
    'type'           => 'text',
    'label'          => 'Linkedin Profile',
    'input'          => 'text',
    'visible'        => true,
    'required'       => false,
    'unique'         => false,
    'note'           => 'Linkedin profile URL',
    'frontend_class' => 'validate-url validate-length maximum-length-250'
];

$installer->addAttribute(
    'customer', 'linkedin_profile', $attributeData
);

$attribute = Mage::getSingleton('eav/config')
    ->getAttribute(
        'customer', 'linkedin_profile'
    );


$installer->addAttributeToGroup(
    $entityTypeId,
    $attributeSetId,
    $attributeGroupId,
    'linkedin_profile',
    '999'
);

$useInForms = [
    'adminhtml_customer',
    'customer_account_create',
    'customer_account_edit',
];

$validateRules = [
    'input_validation' => 'url',
    'max_text_length'  => 250
];

$attribute
    ->setData('used_in_forms', $useInForms)
    ->setData('validate_rules', $validateRules)
    ->setData('is_system', 0)
    ->setData('is_user_defined', 1)
    ->setData('is_visible', 1)
    ->setData('sort_order', 100)
    ->save();


$installer->endSetup();