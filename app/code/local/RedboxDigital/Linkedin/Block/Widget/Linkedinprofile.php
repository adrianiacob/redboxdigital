<?php

class RedboxDigital_Linkedin_Block_Widget_Linkedinprofile extends Mage_Customer_Block_Widget_Abstract
{
    const XML_PATH_LINKEDIN_PROFILE_REQUIRED = 'customer/address/linked_profile_required';

    public function _construct()
    {
        parent::_construct();
        $this->setTemplate('redboxdigital/linkedin/customer/widget/linkedinprofile.phtml');
    }

    public function isEnabled()
    {
        return (bool)$this->_getAttribute('linkedin_profile')->getIsVisible();
    }

    /**
     * replace with Backend admin value
     *
     * @return bool
     *
     */
    public function isRequired()
    {
        return (bool)Mage::getStoreConfigFlag(self::XML_PATH_LINKEDIN_PROFILE_REQUIRED);
    }

    /**
     * Return class validation cases for frontend
     *
     * @return string
     */
    public function getAttributeValidationClass()
    {
        return $this->_getAttribute('linkedin_profile')->getFrontend()->getClass();
    }
}
